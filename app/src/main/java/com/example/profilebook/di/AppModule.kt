package com.example.profilebook.di

import android.app.Application
import androidx.room.Room
import com.example.profilebook.data.data_source.ProfileBookDatabase
import com.example.profilebook.data.repository.ProfileRepositoryImpl
import com.example.profilebook.domain.repository.ProfileRepository
import com.example.profilebook.domain.use_cases.profile_cases.*
import com.example.profilebook.domain.use_cases.user_cases.AddUser
import com.example.profilebook.domain.use_cases.user_cases.GetUser
import com.example.profilebook.domain.use_cases.user_cases.GetUsers
import com.example.profilebook.domain.use_cases.user_cases.UserUseCases
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideProfileBookDatabase(app: Application): ProfileBookDatabase {
        return Room.databaseBuilder(
            app,
            ProfileBookDatabase::class.java,
            ProfileBookDatabase.DATABASE_NAME
        ).build()
    }

    @Provides
    @Singleton
    fun provideProfileRepository(db: ProfileBookDatabase): ProfileRepository {
        return ProfileRepositoryImpl(db.userDao, db.profileDao)
    }

    @Provides
    @Singleton
    fun provideUserUseCases(repository: ProfileRepository): UserUseCases {
        return UserUseCases(
            getUser = GetUser(repository),
            getUsers = GetUsers(repository),
            addUser = AddUser(repository)
        )
    }

    @Provides
    @Singleton
    fun provideProfileUseCases(repository: ProfileRepository): ProfileUseCases {
        return ProfileUseCases(
            addProfile = AddProfile(repository),
            deleteProfile = DeleteProfile(repository),
            getProfile = GetProfile(repository),
            getProfiles = GetProfiles(repository)
        )
    }
}