package com.example.profilebook

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

val prefs: Prefs by lazy {
    ProfileBookApp.prefs!!
}

@HiltAndroidApp
class ProfileBookApp : Application() {

    companion object {
        var prefs: Prefs? = null
        lateinit var instance: ProfileBookApp
        private set
    }

    override fun onCreate() {
        super.onCreate()

        instance = this
        prefs = Prefs(applicationContext)
    }
}