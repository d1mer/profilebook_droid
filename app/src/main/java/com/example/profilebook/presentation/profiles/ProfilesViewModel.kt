package com.example.profilebook.presentation.profiles

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.profilebook.domain.use_cases.profile_cases.ProfileUseCases
import com.example.profilebook.domain.use_cases.user_cases.UserUseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfilesViewModel @Inject constructor(
    private val profileUseCases: ProfileUseCases
) : ViewModel() {

    private val _state = mutableStateOf(ProfileState())
    val state: State<ProfileState> = _state

    private var getProfilesJob: Job? = null

    init {
        getProfiles("login")
    }

    private fun getProfiles(login: String) {
        getProfilesJob?.cancel()
        getProfilesJob = profileUseCases.getProfiles(login)
            .onEach { profiles ->
                _state.value = state.value.copy(
                    profiles = profiles
                )
            }
            .launchIn(viewModelScope)
    }
}