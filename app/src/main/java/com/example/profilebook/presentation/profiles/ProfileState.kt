package com.example.profilebook.presentation.profiles

import com.example.profilebook.domain.models.Profile

data class ProfileState(
    val profiles: List<Profile> = emptyList()
)
