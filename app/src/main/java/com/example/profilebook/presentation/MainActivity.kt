package com.example.profilebook.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.profilebook.ProfileBookApp
import com.example.profilebook.presentation.auth.SignUpScreen
import com.example.profilebook.presentation.auth.signin.SignInScreen
import com.example.profilebook.presentation.profiles.ProfilesScreen
import com.example.profilebook.presentation.util.Screen
import com.example.profilebook.ui.theme.ProfileBookAppTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity()
{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ProfileBookAppTheme {
                Surface(
                    color = MaterialTheme.colors.background
                ) {
                    val navController = rememberNavController()
                    NavHost(
                        navController = navController,
                        startDestination = Screen.SignInScreen.route + "/{$ARG_LOGIN}"
                    ) {
                        composable(
                            route = Screen.SignInScreen.route + "/{$ARG_LOGIN}",
                            arguments = listOf(
                                navArgument(
                                    name = "$ARG_LOGIN"
                                ) {
                                    type = NavType.StringType
                                    defaultValue = ""
                                    nullable = true
                                },
                            )
                        ) {
                            SignInScreen(navController = navController)
                        }
                        composable(
                            route = Screen.SignUpScreen.route
                        ) {
                            SignUpScreen(navController = navController)
                        }
                        composable(
                            route = Screen.ProfilesScreen.route
                        ) {
                            ProfilesScreen(navController = navController)
                        }
                    }
                }
            }
        }
    }

    companion object {
        const val ARG_LOGIN = "login"
    }
}