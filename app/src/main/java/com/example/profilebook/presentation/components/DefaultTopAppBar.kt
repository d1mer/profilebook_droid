package com.example.profilebook.presentation.components

import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import com.example.profilebook.ui.theme.LightBlue

@Composable
fun DefaultTopAppBar(
    title: String,
    navController: NavController
) {
    TopAppBar(
        backgroundColor = LightBlue,
        title = { Text(title) },
        navigationIcon = if(navController.previousBackStackEntry != null) {
            {
                IconButton(onClick = { navController.navigateUp() }) {
                    Icon(imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "Back"
                    )
                }
            }
        } else { null }
    )
}