package com.example.profilebook.presentation.auth.signin

sealed class SignInEvent {
    data class LoginChanged(val login: String): SignInEvent()
    data class PasswordChanged(val password: String): SignInEvent()

    object Submit: SignInEvent()
}
