package com.example.profilebook.presentation.util

sealed class Screen(val route: String) {
    object SignInScreen: Screen("sign_in_screen")
    object SignUpScreen: Screen("sign_up_screen")
    object ProfilesScreen: Screen("profiles_screen")

    fun withArgs(vararg args: String?): String {
        return buildString {
            append(route)
            args.forEach {  arg ->
                append("/$arg")
            }
        }
    }
}