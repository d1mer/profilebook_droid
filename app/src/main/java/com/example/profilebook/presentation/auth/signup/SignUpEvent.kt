package com.example.profilebook.presentation.auth.signup

sealed class SignUpEvent {
    data class LoginChanged(val login: String): SignUpEvent()
    data class PasswordChanged(val password: String): SignUpEvent()
    data class ConfirmPasswordChanged(val confirmPassword: String): SignUpEvent()

    object Submit: SignUpEvent()
}
