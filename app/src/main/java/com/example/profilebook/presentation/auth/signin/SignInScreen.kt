package com.example.profilebook.presentation.auth.signin

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.profilebook.R
import com.example.profilebook.UiText
import com.example.profilebook.presentation.components.DefaultTopAppBar
import com.example.profilebook.presentation.util.Screen
import com.example.profilebook.ui.theme.Orange

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun SignInScreen(
    navController: NavController,
    viewModel: SignInViewModel = hiltViewModel()
) {

    val state = viewModel.state
    val context = LocalContext.current

    LaunchedEffect(key1 = context) {
        viewModel.validationEvents.collect { event ->
            when (event) {
                is SignInViewModel.ValidationEvent.Success -> {
                    Toast.makeText(
                        context,
                        "Log in successful",
                        Toast.LENGTH_LONG
                    ).show()

                    val sharedPref = context.getSharedPreferences(
                        UiText.StringResource(R.string.preference_file_key).asString(context),
                        Context.MODE_PRIVATE
                    )

                    with(sharedPref.edit()) {
                        putString(
                            UiText.StringResource(R.string.saved_user_login).asString(context),
                            event.login
                        )
                        apply()
                    }

                    navController.navigate(
                        Screen.ProfilesScreen.route
                    ) {
                        popUpTo(0)
                    }
                }
            }
        }
    }

    Scaffold(
        topBar = {
            DefaultTopAppBar(
                stringResource(R.string.sign_in_page_title),
                navController
            )
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(32.dp),
            verticalArrangement = Arrangement.Center
        ) {
            TextField(
                value = state.login,
                onValueChange = {
                    viewModel.onEvent(SignInEvent.LoginChanged(it))
                },
                isError = state.loginError != null,
                modifier = Modifier.fillMaxWidth(),
                placeholder = {
                    Text(text = stringResource(R.string.login_hint))
                }
            )
            if (state.loginError != null) {
                Text(
                    text = state.loginError.asString(LocalContext.current),
                    color = MaterialTheme.colors.error,
                    modifier = Modifier.align(Alignment.End)
                )
            }
            Spacer(modifier = Modifier.height(24.dp))

            TextField(
                value = state.password,
                onValueChange = {
                    viewModel.onEvent(SignInEvent.PasswordChanged(it))
                },
                isError = state.passwordError != null,
                modifier = Modifier.fillMaxWidth(),
                placeholder = {
                    Text(text = stringResource(R.string.password_hint))
                },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Password
                ),
                visualTransformation = PasswordVisualTransformation()
            )
            if (state.passwordError != null) {
                Text(
                    text = state.passwordError.asString(LocalContext.current),
                    color = MaterialTheme.colors.error,
                    modifier = Modifier.align(Alignment.End)
                )
            }
            Spacer(modifier = Modifier.height(24.dp))
        }
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(32.dp),
            verticalArrangement = Arrangement.Bottom
        ) {
            Button(
                onClick = {
                    viewModel.onEvent(SignInEvent.Submit)
                },
                colors = ButtonDefaults.buttonColors(backgroundColor = Orange),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(60.dp)
            ) {
                Text(text = stringResource(R.string.button_sign_in))
            }
            Spacer(modifier = Modifier.height(16.dp))
            TextButton(
                onClick = { navController.navigate(Screen.SignUpScreen.route) },
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
            ) {
                Text(text = stringResource(R.string.button_sign_up), textDecoration = TextDecoration.Underline)
            }
            Spacer(modifier = Modifier.height(48.dp))
        }
    }
}