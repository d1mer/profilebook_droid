package com.example.profilebook.presentation.auth.signup

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.profilebook.domain.models.InvalidUserException
import com.example.profilebook.domain.models.User
import com.example.profilebook.domain.use_cases.user_cases.UserUseCases
import com.example.profilebook.domain.use_cases.validation.signup.ValidateConfirmPassword
import com.example.profilebook.domain.use_cases.validation.signup.ValidateLogin
import com.example.profilebook.domain.use_cases.validation.signup.ValidatePassword
import com.example.profilebook.presentation.auth.signin.SignInViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.schedule

@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val userUseCases: UserUseCases
) : ViewModel() {

    private val validateLogin: ValidateLogin = ValidateLogin(userUseCases)
    private val validatePassword: ValidatePassword = ValidatePassword()
    private val validateConfirmPassword: ValidateConfirmPassword = ValidateConfirmPassword()

    var state by mutableStateOf(SignUpState())

    private val validationEventChannel = Channel<SignUpViewModel.ValidationEvent>()
    val validationEvents = validationEventChannel.receiveAsFlow()

    private var currentNoteId: Int? = null
    private var user: User? = null

    fun onEvent(event: SignUpEvent) {
        when(event) {
            is SignUpEvent.LoginChanged -> {
                state = state.copy(login = event.login)
                state = state.copy(loginError = null)
            }
            is SignUpEvent.PasswordChanged -> {
                state = state.copy(password = event.password)
                state = state.copy(passwordError = null)
                state = state.copy(confirmPasswordError = null)
            }
            is SignUpEvent.ConfirmPasswordChanged -> {
                state = state.copy(confirmPassword = event.confirmPassword)
                state = state.copy(passwordError = null)
                state = state.copy(confirmPasswordError = null)
            }
            is SignUpEvent.Submit -> {
                viewModelScope.launch {
                    user = userUseCases.getUser(state.login)
                }
                Timer().schedule(150){
                    submitData(user)
                }

            }
        }
    }

    private fun submitData(user: User?) {

        val loginResult = validateLogin.execute(state.login, user)
        val passwordResult = validatePassword.execute(state.password)
        val confirmPasswordResult = validateConfirmPassword.execute(state.password, state.confirmPassword)

        val hasError = listOf(
            loginResult,
            passwordResult,
            confirmPasswordResult
        ).any { !it.successful }

        if(hasError) {
            state = state.copy(
                loginError = loginResult.errorMessage,
                passwordError = passwordResult.errorMessage,
                confirmPasswordError = confirmPasswordResult.errorMessage
            )
            return
        } else {
            viewModelScope.launch {
                validationEventChannel.send(ValidationEvent.Success)

                try {
                    userUseCases.addUser(
                        User(
                            login = state.login,
                            password = state.password,
                            id = currentNoteId
                        )
                    )
                    state = state.copy(
                        loginError = null,
                        passwordError = null,
                        confirmPasswordError = null
                    )
                    validationEventChannel.send(ValidationEvent.SaveUser(state.login))
                } catch (e: Exception) {
                    validationEventChannel.send(ValidationEvent.ShowSnackbar(
                        message = e.message ?: "Couldn't save user")
                    )
                }
            }
        }
    }

    sealed class ValidationEvent {
        object Success: ValidationEvent()
        data class ShowSnackbar(val message: String): ValidationEvent()
        data class SaveUser(val login: String): ValidationEvent()
    }
}