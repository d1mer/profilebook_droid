package com.example.profilebook.presentation.auth.signup

import com.example.profilebook.UiText

data class SignUpState(
    val login: String = "",
    val loginError: UiText.StringResource? = null,
    val password: String = "",
    val passwordError: UiText.StringResource? = null,
    val confirmPassword: String = "",
    val confirmPasswordError: UiText.StringResource? = null
)
