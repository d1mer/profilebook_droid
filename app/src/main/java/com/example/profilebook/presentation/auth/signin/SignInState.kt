package com.example.profilebook.presentation.auth.signin

import com.example.profilebook.UiText

data class SignInState(
    val login: String = "",
    val loginError: UiText.StringResource? = null,
    val password: String = "",
    val passwordError: UiText.StringResource? = null
)
