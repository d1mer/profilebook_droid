package com.example.profilebook.presentation.auth

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.Green
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.profilebook.R
import com.example.profilebook.UiText
import com.example.profilebook.presentation.auth.signup.SignUpEvent
import com.example.profilebook.presentation.auth.signup.SignUpViewModel
import com.example.profilebook.presentation.components.DefaultTopAppBar
import com.example.profilebook.presentation.util.Screen
import com.example.profilebook.ui.theme.Orange

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun SignUpScreen(
    navController: NavController,
    viewModel: SignUpViewModel = hiltViewModel()
) {
    val scaffoldState = rememberScaffoldState()
    val scope = rememberCoroutineScope()

    val state = viewModel.state
    val context = LocalContext.current



    LaunchedEffect(key1 = context) {
        viewModel.validationEvents.collect { event ->
            when(event) {
                is SignUpViewModel.ValidationEvent.Success -> {
                    scaffoldState.snackbarHostState.showSnackbar(
                        actionLabel = "OK",
                        message = UiText.StringResource(R.string.authetication_success).asString(context),
                    )
                }
                is SignUpViewModel.ValidationEvent.ShowSnackbar -> {
                    scaffoldState.snackbarHostState.showSnackbar(
                        actionLabel = "OK",
                        message = event.message,
                    )
                }
                is SignUpViewModel.ValidationEvent.SaveUser -> {
                    navController.navigate(
                        Screen.SignInScreen.route + "/${event.login}"
                    ) {
                        popUpTo(0)
                    }
                }
            }
        }
    }

    Scaffold(
        topBar = {
            DefaultTopAppBar(
                stringResource(R.string.sign_up_page_title),
                navController
            )
        },
        scaffoldState = scaffoldState,
        snackbarHost = {
            SnackbarHost(it) { data ->
                Snackbar(
                    actionColor = Green,
                    snackbarData = data
                )
            }
        },
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(32.dp),
            verticalArrangement = Arrangement.Center
        ) {
            TextField(
                value = state.login,
                onValueChange = {
                    viewModel.onEvent(SignUpEvent.LoginChanged(it))
                },
                isError = state.loginError != null,
                modifier = Modifier.fillMaxWidth(),
                placeholder = {
                    Text(text = stringResource(R.string.login_hint))
                }
            )
            if(state.loginError != null) {
                Text(
                    text = state.loginError.asString(LocalContext.current),
                    color = MaterialTheme.colors.error,
                    modifier = Modifier.align(Alignment.End)
                )
            }
            Spacer(modifier = Modifier.height(24.dp))

            TextField(
                value = state.password,
                onValueChange = {
                    viewModel.onEvent(SignUpEvent.PasswordChanged(it))
                },
                isError = state.passwordError != null,
                modifier = Modifier.fillMaxWidth(),
                placeholder = {
                    Text(text = stringResource(R.string.password_hint))
                },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Password
                ),
                visualTransformation = PasswordVisualTransformation()
            )
            if(state.passwordError != null) {
                Text(
                    text = state.passwordError.asString(LocalContext.current),
                    color = MaterialTheme.colors.error,
                    modifier = Modifier.align(Alignment.End)
                )
            }
            Spacer(modifier = Modifier.height(24.dp))

            TextField(
                value = state.confirmPassword,
                onValueChange = {
                    viewModel.onEvent(SignUpEvent.ConfirmPasswordChanged(it))
                },
                isError = state.confirmPasswordError != null,
                modifier = Modifier.fillMaxWidth(),
                placeholder = {
                    Text(text = stringResource(R.string.password_confirm_hint))
                },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Password
                ),
                visualTransformation = PasswordVisualTransformation()
            )
            if(state.confirmPasswordError != null) {
                Text(
                    text = state.confirmPasswordError.asString(LocalContext.current),
                    color = MaterialTheme.colors.error,
                    modifier = Modifier.align(Alignment.End)
                )
            }
            Spacer(modifier = Modifier.height(16.dp))
        }
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(32.dp),
            verticalArrangement = Arrangement.Bottom
        ) {
            Button(onClick = {
                viewModel.onEvent(SignUpEvent.Submit)
            },

                colors = ButtonDefaults.buttonColors(backgroundColor = Orange),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(60.dp)
            ) {
                Text(text = stringResource(R.string.button_sign_up))
            }
            Spacer(modifier = Modifier.height(48.dp))
        }
    }
}