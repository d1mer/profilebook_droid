package com.example.profilebook.presentation.auth.signin

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.profilebook.domain.models.User
import com.example.profilebook.domain.use_cases.user_cases.UserUseCases
import com.example.profilebook.domain.use_cases.validation.signin.ValidateLogin
import com.example.profilebook.domain.use_cases.validation.signin.ValidatePassword
import com.example.profilebook.presentation.MainActivity.Companion.ARG_LOGIN
import com.example.profilebook.presentation.auth.signup.SignUpViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.schedule

@HiltViewModel
class SignInViewModel @Inject constructor(
    private val userUseCases: UserUseCases,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val validateLogin: ValidateLogin = ValidateLogin()
    private val validatePassword: ValidatePassword = ValidatePassword()

    var state by mutableStateOf(SignInState())

    private val validationEventChannel = Channel<SignInViewModel.ValidationEvent>()
    val validationEvents = validationEventChannel.receiveAsFlow()

    private var user: User? = null

    init {
        savedStateHandle.get<String?>("login")?.let { login ->
            state = state.copy(login = login)
        }
    }

    fun onEvent(event: SignInEvent) {
        when(event) {
            is SignInEvent.LoginChanged -> {
                state = state.copy(login = event.login)
                state = state.copy(loginError = null)
            }
            is SignInEvent.PasswordChanged -> {
                state = state.copy(password = event.password)
                state = state.copy(passwordError = null)
            }
            is SignInEvent.Submit -> {
                viewModelScope.launch {
                    user = userUseCases.getUser(state.login)
                }
                Timer().schedule(150){
                    submitData(user)
                }
            }
        }
    }

    private fun submitData(user: User?) {
        val loginResult = validateLogin.execute(state.login, user)
        val passwordResult = validatePassword.execute(state.password, user)

        val hasError = listOf(
            loginResult,
            passwordResult
        ).any { !it.successful }

        if(hasError) {
            state = state.copy(
                loginError = loginResult.errorMessage,
                passwordError = passwordResult.errorMessage
            )
            return
        }

        viewModelScope.launch {
            validationEventChannel.send(SignInViewModel.ValidationEvent.Success(state.login))
        }
    }

    sealed class ValidationEvent {
        data class Success(val login: String): ValidationEvent()
    }
}