package com.example.profilebook.presentation.profiles

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController

@Composable
fun ProfilesScreen(
    navController: NavController,
    viewModel: ProfilesViewModel = hiltViewModel()
) {
    val state = viewModel.state
    val context = LocalContext.current
}