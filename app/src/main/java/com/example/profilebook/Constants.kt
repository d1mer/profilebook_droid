package com.example.profilebook

object Constants {
    const val MIN_LOGIN_LENGTH = 4
    const val MAX_LOGIN_LENGTH = 16
    const val MIN_PASSWORD_LENGTH = 8
    const val MAX_PASSWORD_LENGTH = 16
}