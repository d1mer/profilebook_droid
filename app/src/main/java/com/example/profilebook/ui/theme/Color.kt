package com.example.profilebook.ui.theme

import androidx.compose.ui.graphics.Color

val LightBlue = Color(0xFF2096F3)
val Black = Color(0xFF000000)
val White = Color(0xFFFFFFFF)
val DarkOrange = Color(0xD0692A)
val SignUpLink = Color(0x4D54F9)
val DarkGray = Color(0xFF202020)
val Orange = Color(0xFFD06A2A)
val LightGray = Color(0xC2C2C2)