package com.example.profilebook.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private val DarkColorPalette = darkColors(
    primary = Color.White,
    background = com.example.profilebook.ui.theme.DarkGray,
    onBackground = Color.White,
    surface = LightBlue,
    onSurface = com.example.profilebook.ui.theme.DarkGray
)

private val LightColorPalette = lightColors(
    primary = LightBlue,
    background = Color.White,
    onBackground = Black,
    surface = Color.LightGray,
    secondary = Orange
)

@Composable
fun ProfileBookAppTheme(darkTheme: Boolean = false, content: @Composable() () -> Unit) {
    MaterialTheme(
        colors = LightColorPalette,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}