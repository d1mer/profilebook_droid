package com.example.profilebook

import android.content.Context
import android.content.SharedPreferences
import androidx.compose.ui.res.stringResource

class Prefs(context: Context) {

    private val userLoginKey = UiText.StringResource(R.string.saved_user_login).asString(context)

    private val preferences: SharedPreferences = context.getSharedPreferences(
        UiText.StringResource(R.string.preference_file_key).asString(context),
        Context.MODE_PRIVATE
    )

    var userLogin: String?
    get() = preferences.getString(
        userLoginKey,
        ""
    )
    set(value) = preferences.edit().putString(
        userLoginKey,
        value
    ).apply()
}