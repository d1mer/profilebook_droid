package com.example.profilebook.domain.use_cases.profile_cases

data class ProfileUseCases(
    val addProfile: AddProfile,
    val deleteProfile: DeleteProfile,
    val getProfile: GetProfile,
    val getProfiles: GetProfiles
)
