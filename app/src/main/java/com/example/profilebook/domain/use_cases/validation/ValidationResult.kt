package com.example.profilebook.domain.use_cases.validation

import com.example.profilebook.UiText

data class ValidationResult(
    val successful: Boolean,
    val errorMessage: UiText.StringResource? = null
)
