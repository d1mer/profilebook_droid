package com.example.profilebook.domain.use_cases.user_cases

data class UserUseCases(
    val getUser: GetUser,
    val getUsers: GetUsers,
    val addUser: AddUser
)
