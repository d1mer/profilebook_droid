package com.example.profilebook.domain.use_cases.validation.signup

import androidx.hilt.work.HiltWorker
import com.example.profilebook.Constants.MAX_LOGIN_LENGTH
import com.example.profilebook.Constants.MIN_LOGIN_LENGTH
import com.example.profilebook.R
import com.example.profilebook.UiText
import com.example.profilebook.domain.models.User
import com.example.profilebook.domain.use_cases.user_cases.UserUseCases
import com.example.profilebook.domain.use_cases.validation.ValidationResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.schedule

class ValidateLogin(
    val userUseCases: UserUseCases
){

    fun execute(login: String, user: User?): ValidationResult {

        if(login.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(R.string.login_is_blank)
            )
        }

        if(login.length < MIN_LOGIN_LENGTH || login.length > MAX_LOGIN_LENGTH) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(
                    R.string.login_length_hint,
                    MIN_LOGIN_LENGTH,
                    MAX_LOGIN_LENGTH
                )
            )
        }

        if(login[0].isDigit()) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(R.string.login_start_digit)
            )
        }

        if(login == user?.login) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(R.string.login_taken)
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}