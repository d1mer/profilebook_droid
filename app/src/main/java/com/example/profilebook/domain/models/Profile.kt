package com.example.profilebook.domain.models

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.profilebook.UiText

@Entity(tableName = "profiles")
data class Profile (
    @PrimaryKey val id: Int,
    @NonNull @ColumnInfo(name = "nick_name") val nickName: String,
    @NonNull @ColumnInfo(name = "name") val name: String,
    @NonNull @ColumnInfo(name = "image") val image: String,
    @NonNull @ColumnInfo(name = "owner") val owner: String,
    @NonNull @ColumnInfo(name = "creation_time") val creationTime: Int,
    @ColumnInfo(name = "description") val description: String,
)

class InvalidProfileException(message: String): Exception(message)