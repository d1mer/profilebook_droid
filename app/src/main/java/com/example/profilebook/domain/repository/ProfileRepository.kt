package com.example.profilebook.domain.repository

import android.content.SharedPreferences
import androidx.compose.runtime.Composable
import com.example.profilebook.domain.models.Profile
import com.example.profilebook.domain.models.User
import kotlinx.coroutines.flow.Flow

interface ProfileRepository {

    fun getUsers(): Flow<List<User>>

    suspend fun getUserByLogin(login: String): User?

    suspend fun insertUser(user: User)

    fun getProfilesByUser(login: String): Flow<List<Profile>>

    suspend fun getProfileById(id: Int): Profile?

    suspend fun insertProfile(profile: Profile)

    suspend fun deleteProfile(profile: Profile)
}