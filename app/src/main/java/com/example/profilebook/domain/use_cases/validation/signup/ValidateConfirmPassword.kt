package com.example.profilebook.domain.use_cases.validation.signup

import com.example.profilebook.R
import com.example.profilebook.UiText
import com.example.profilebook.domain.use_cases.validation.ValidationResult

class ValidateConfirmPassword {

    fun execute(password: String, confirmPassword: String): ValidationResult {

        if(password != confirmPassword) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(R.string.password_not_much)
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}