package com.example.profilebook.domain.use_cases.profile_cases

import android.content.res.Resources
import com.example.profilebook.R
import com.example.profilebook.domain.models.InvalidProfileException
import com.example.profilebook.domain.models.Profile
import com.example.profilebook.domain.repository.ProfileRepository

class AddProfile(
    private val repository: ProfileRepository
) {

    @Throws(InvalidProfileException::class)
    suspend operator fun invoke(profile: Profile) {
        if(profile.image.isBlank()) {
            throw InvalidProfileException("The image can't be empty")
        }
        if(profile.name.isBlank()) {
            throw InvalidProfileException("The name can't be empty")
        }
        if(profile.nickName.isBlank()) {
            throw InvalidProfileException("The nickname can't be empty")
        }

        repository.insertProfile(profile)
    }
}