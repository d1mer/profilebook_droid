package com.example.profilebook.domain.use_cases.user_cases

import com.example.profilebook.domain.models.InvalidUserException
import com.example.profilebook.domain.models.User
import com.example.profilebook.domain.repository.ProfileRepository

class AddUser(
    private val repository: ProfileRepository
) {

    suspend operator fun invoke(user: User) {
        repository.insertUser(user)
    }
}