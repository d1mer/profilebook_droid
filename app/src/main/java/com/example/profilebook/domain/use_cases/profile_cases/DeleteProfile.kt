package com.example.profilebook.domain.use_cases.profile_cases

import com.example.profilebook.domain.models.Profile
import com.example.profilebook.domain.repository.ProfileRepository

class DeleteProfile(
    private val repository: ProfileRepository
) {

    suspend operator fun invoke(profile: Profile) {
        repository.deleteProfile(profile)
    }
}