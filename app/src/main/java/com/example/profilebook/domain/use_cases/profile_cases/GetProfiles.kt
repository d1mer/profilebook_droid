package com.example.profilebook.domain.use_cases.profile_cases

import com.example.profilebook.domain.models.Profile
import com.example.profilebook.domain.repository.ProfileRepository
import kotlinx.coroutines.flow.Flow

class GetProfiles(
    private val repository: ProfileRepository
) {

    operator fun invoke(login: String): Flow<List<Profile>> {
        return repository.getProfilesByUser(login)
    }
}