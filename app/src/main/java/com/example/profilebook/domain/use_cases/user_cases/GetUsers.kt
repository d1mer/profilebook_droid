package com.example.profilebook.domain.use_cases.user_cases

import com.example.profilebook.domain.models.User
import com.example.profilebook.domain.repository.ProfileRepository
import kotlinx.coroutines.flow.Flow

class GetUsers(
    private val repository: ProfileRepository
) {

    operator fun invoke(): Flow<List<User>> {
        return repository.getUsers()
    }
}