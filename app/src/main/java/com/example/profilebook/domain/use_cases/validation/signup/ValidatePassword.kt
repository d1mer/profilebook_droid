package com.example.profilebook.domain.use_cases.validation.signup

import com.example.profilebook.Constants.MAX_PASSWORD_LENGTH
import com.example.profilebook.Constants.MIN_PASSWORD_LENGTH
import com.example.profilebook.R
import com.example.profilebook.UiText
import com.example.profilebook.domain.use_cases.validation.ValidationResult

class ValidatePassword {

    fun execute(password: String): ValidationResult {

        if(password.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(R.string.password_is_blank)
            )
        }

        if(password.length < MIN_PASSWORD_LENGTH || password.length > MAX_PASSWORD_LENGTH) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(
                    R.string.password_length,
                    MIN_PASSWORD_LENGTH,
                    MAX_PASSWORD_LENGTH
                )
            )
        }

        val containsLettersAndDigits = password.any {it.isDigit()}
                && password.any { it.isLowerCase() }
                && password.any { it.isUpperCase() }

        if(!containsLettersAndDigits) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(R.string.password_must_contain)
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}