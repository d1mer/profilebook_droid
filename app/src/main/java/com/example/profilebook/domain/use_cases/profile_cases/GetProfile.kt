package com.example.profilebook.domain.use_cases.profile_cases

import com.example.profilebook.domain.models.Profile
import com.example.profilebook.domain.repository.ProfileRepository

class GetProfile(
    private val repository: ProfileRepository
) {
    suspend operator fun invoke(id: Int) : Profile? {
        return repository.getProfileById(id)
    }
}