package com.example.profilebook.domain.use_cases.validation.signin

import com.example.profilebook.Constants
import com.example.profilebook.R
import com.example.profilebook.UiText
import com.example.profilebook.domain.models.User
import com.example.profilebook.domain.use_cases.validation.ValidationResult

class ValidatePassword {

    fun execute(password: String, user: User?): ValidationResult {

        if(password.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(R.string.password_is_blank)
            )
        }

        if(password.length < Constants.MIN_PASSWORD_LENGTH || password.length > Constants.MAX_PASSWORD_LENGTH) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(
                    R.string.password_length,
                    Constants.MIN_PASSWORD_LENGTH,
                    Constants.MAX_PASSWORD_LENGTH
                )
            )
        }

        val containsLettersAndDigits = password.any {it.isDigit()}
                && password.any { it.isLowerCase() }
                && password.any { it.isUpperCase() }

        if(!containsLettersAndDigits) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(R.string.password_must_contain)
            )
        }

        if (user == null) {
            return ValidationResult(
                successful = false
            )
        }

        if (user.password != password) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(R.string.password_wrong)
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}