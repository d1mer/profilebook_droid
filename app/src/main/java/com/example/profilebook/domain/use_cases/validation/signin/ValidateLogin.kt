package com.example.profilebook.domain.use_cases.validation.signin

import com.example.profilebook.Constants
import com.example.profilebook.R
import com.example.profilebook.UiText
import com.example.profilebook.domain.models.User
import com.example.profilebook.domain.use_cases.validation.ValidationResult

class ValidateLogin {

    fun execute(login:String, user: User?): ValidationResult {

        if(login.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(R.string.login_is_blank)
            )
        }

        if(login.length < Constants.MIN_LOGIN_LENGTH || login.length > Constants.MAX_LOGIN_LENGTH) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(
                    R.string.login_length_hint,
                    Constants.MIN_LOGIN_LENGTH,
                    Constants.MAX_LOGIN_LENGTH
                )
            )
        }

        if(login[0].isDigit()) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(R.string.login_start_digit)
            )
        }

        if(user == null) {
            return ValidationResult(
                successful = false,
                errorMessage = UiText.StringResource(R.string.login_not_found)
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}