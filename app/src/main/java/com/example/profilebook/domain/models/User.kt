package com.example.profilebook.domain.models

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "users",
indices = [Index(value = ["login"], unique = true)])
data class User (
    @PrimaryKey val id: Int?,
    @NonNull @ColumnInfo(name = "login") val login: String,
    @NonNull @ColumnInfo(name = "password") val password: String,
)

class InvalidUserException(message: String): Exception(message)