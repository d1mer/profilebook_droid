package com.example.profilebook.domain.use_cases.user_cases

import com.example.profilebook.domain.models.User
import com.example.profilebook.domain.repository.ProfileRepository

class GetUser(
    private val repository: ProfileRepository
) {

    suspend operator fun invoke(login: String): User? {
        return repository.getUserByLogin(login)
    }
}