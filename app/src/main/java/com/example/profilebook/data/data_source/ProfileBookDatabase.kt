package com.example.profilebook.data.data_source

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.profilebook.domain.models.Profile
import com.example.profilebook.domain.models.User

@Database(
    entities = [(User::class), (Profile::class)],
    version = 2
)
abstract class ProfileBookDatabase: RoomDatabase() {

    abstract val userDao: UserDao
    abstract val profileDao: ProfileDao

    companion object {
        const val DATABASE_NAME = "profiles_db"
    }
}