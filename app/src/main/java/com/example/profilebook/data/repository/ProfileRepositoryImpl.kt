package com.example.profilebook.data.repository

import android.content.Context
import android.content.SharedPreferences
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import com.example.profilebook.R
import com.example.profilebook.UiText
import com.example.profilebook.data.data_source.ProfileDao
import com.example.profilebook.data.data_source.UserDao
import com.example.profilebook.domain.models.Profile
import com.example.profilebook.domain.models.User
import com.example.profilebook.domain.repository.ProfileRepository
import kotlinx.coroutines.flow.Flow

class ProfileRepositoryImpl(
    private val userDao: UserDao,
    private val profileDao: ProfileDao
) : ProfileRepository {

    override fun getUsers(): Flow<List<User>> {
        return userDao.getUsers()
    }

    override suspend fun getUserByLogin(login: String): User? {
        return userDao.getUserByLogin(login)
    }

    override suspend fun insertUser(user: User) {
        userDao.insertUser(user)
    }

    override fun getProfilesByUser(login: String): Flow<List<Profile>> {
        return profileDao.getProfilesByUser(login)
    }

    override suspend fun getProfileById(id: Int): Profile? {
        return profileDao.getProfileById(id)
    }

    override suspend fun insertProfile(profile: Profile) {
        profileDao.insertProfile(profile)
    }

    override suspend fun deleteProfile(profile: Profile) {
        profileDao.deleteProfile(profile)
    }
}